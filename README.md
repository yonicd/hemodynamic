
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Hemodynamic

### Project Package

An R package was created to hold all the datasets and script used thus
far in the study. The package is called `hemodynamic`.

#### Installation

``` r
# install.packages('remotes')
# remotes::install_github('r-lib/devtools')

devtools::install_gitlab('yonicd/hemodynamic')
```

#### Usage

``` r
library(hemodynamic)
```

### CIMDO

#### Status

  - An attempt to reconstruct the extended CIMDO algorithm was carried
    out. The extension to the algorithm was to add additional areas of
    constraint on the functional optimization to include extreme
    increases of the multivariate distribution.

  - A mapping of the constraint regions was attained.

  - Ability to return the joint probability distribribution equal to 1
    was not attained.

  - The CIMDO copula was not reconstructed.

#### Problems with using CIMDO

  - Attain as many points before start of the patient in pero-operative
    state to estimate what patient specific extremes are.

  - Current algorithm does not support irregular time periods and
    unbalanced data structures.

  - Get more high frequency measurements before/during procedure to
    create richer multivariate distribution.

### Stability Index

A stability index was evaluated using Heart Rate and the Ratio of
Diastolic/Systolic. Denoting an observation at time t \(x_t\), the rate
of change of calculated \(y_t = x_t/x_{t-1}\). The rolling mean and
standard deviation of \(y_t\) was evaluated and then for each time
period were scaled to 0 mean 1 sd and the pdf was calculated.

``` r
hemodynamic::survey_raw%>%
  hemodynamic::stability()%>%
  dplyr::filter(measure=='bp')%>%
  ggplot2::ggplot(ggplot2::aes(x=times,y=value,colour=stat)) + 
  ggplot2::geom_smooth(method = 'loess',se = FALSE) + 
  ggplot2::facet_wrap(~cases,scales = 'free_x') +
  ggplot2::theme(legend.position = 'bottom')
```

![](tools/readme/README-unnamed-chunk-4-1.png)<!-- -->

``` r

hemodynamic::survey_raw%>%
  hemodynamic::stability()%>%
  dplyr::filter(measure=='bp')%>%
  ggplot2::ggplot(ggplot2::aes(x=times,y=value,colour=stat)) + 
  ggplot2::geom_smooth(method = 'loess',se = FALSE) + 
  ggplot2::geom_point(alpha = 0.1) + 
  ggplot2::facet_wrap(~cases,scales = 'free_x') +
  ggplot2::theme(legend.position = 'bottom')
```

![](tools/readme/README-unnamed-chunk-4-2.png)<!-- -->

### Mapping Cases

[read\_case\_data.R](data-raw/read_case_data.R)

``` r
curated_data <- hemodynamic::survey_curated%>%
  dplyr::mutate(
      Date = as.POSIXct(
          strptime(Date,"%d/%m/%Y %T")
        )
    )%>%
  dplyr::filter(Case%in%c(1,2,7,10,15))%>%
  dplyr::mutate(Case=factor(Case,labels=c(425886,426528,424838,427105,423841)))

raw_data <- hemodynamic::survey_raw%>%
  dplyr::filter(cases%in%c(425886,426528,424838,427105,423841))%>%
  dplyr::mutate(
    cases=factor(cases),
    Date=as.POSIXct(strptime(paste(date,times),"%d/%m/%Y %T")),
    m.label=toupper(m.label)
    )%>%
  dplyr::rename(
    Value=mv,
    Variable=m.label)%>%
  dplyr::select(Case=cases,Date,Variable,Value)

combined <- dplyr::bind_rows(
  curated_data,
  raw_data)%>%
  dplyr::distinct()

combined%>%
  dplyr::filter(Value>20)%>%
  ggplot2::ggplot(
    ggplot2::aes(
      x = Date,
      y = Value,
      colour = Variable)) +
  ggplot2::theme_bw() +
  ggplot2::geom_line(show.legend = FALSE) +
  ggplot2::facet_grid(Variable~Case, scales = "free") +
  ggplot2::theme(legend.position = "top",
                 axis.text.x = ggplot2::element_text(
                   angle = 45
                 ))
```

![](tools/readme/README-unnamed-chunk-5-1.png)<!-- -->

### Stability Index

## Data

script to create [package data](data-raw/read_data.R):

``` r
hemodynamic::PtInfo
#> # A tibble: 151 x 8
#>    Case_Date           Seq_nr New50_Over4Hr   Age ASA   Weight
#>    <dttm>               <dbl>         <dbl> <dbl> <chr>  <dbl>
#>  1 2010-01-06 00:00:00      3             0    69 2       69.9
#>  2 2010-01-06 00:00:00     10             0    27 2       93  
#>  3 2010-01-15 00:00:00     76             0    54 2       51.7
#>  4 2010-01-21 00:00:00      2             0    51 4E      65  
#>  5 2010-01-21 00:00:00     35             0    32 2       85.7
#>  6 2010-01-21 00:00:00     91             1    71 3       98  
#>  7 2010-01-26 00:00:00     16             1    88 3       61  
#>  8 2010-02-02 00:00:00    147             0    59 2      109  
#>  9 2010-02-03 00:00:00     14             0    51 4E      65  
#> 10 2010-02-05 00:00:00     73             1    62 3       84  
#> # ... with 141 more rows, and 2 more variables: Sched_Proc_Desc <chr>,
#> #   Preop_Dx <chr>

hemodynamic::OUTCOME
#> # A tibble: 160 x 15
#>    ACCT_ID CASE_ID  DIED IP_OP CASE_VOL   LOS ICU_DAYS ICD9_ADMIT
#>      <int> <chr>   <int> <chr>    <int> <int>    <int> <chr>     
#>  1       5 150         1 IP          NA    37        5 99681     
#>  2      31 120         1 IP           1     8        3 4241      
#>  3      35 115         1 OP           1     0        0 2859      
#>  4      46 2           1 IP          NA    37        0 99682     
#>  5     107 38          1 OP           1     0        0 7842      
#>  6     109 4           1 OP           1     0        0 36616     
#>  7     114 62          1 IP          NA     7        0 7802      
#>  8     117 13          1 IP          NA    26       26 78652     
#>  9     135 8           1 IP          NA    13        1 1977      
#> 10     137 41          1 IP           1     2        0 9972      
#> # ... with 150 more rows, and 7 more variables: ICD9_ADMIT_LABEL <chr>,
#> #   ICD9_CODE_OUTCOME <chr>, CARDIAC <int>, BRAIN <int>, KIDNEY <int>,
#> #   LIVER <int>, PULMONARY <int>

hemodynamic::OR
#> # A tibble: 96,488 x 5
#>     case obs_index time   variable   value
#>    <int>     <int> <time> <chr>      <int>
#>  1     1        85 10:07  NIBP Dia      58
#>  2     1        85 10:07  NIBP Sys     120
#>  3     1        85 10:08  Heart rate    67
#>  4     1        85 10:09  Heart rate    66
#>  5     1        85 10:09  NIBP Dia      61
#>  6     1        85 10:09  NIBP Sys     126
#>  7     1        85 10:10  Heart rate    65
#>  8     1        85 10:11  Heart rate    78
#>  9     1        85 10:12  NIBP Dia      50
#> 10     1        85 10:12  NIBP Sys      85
#> # ... with 96,478 more rows

hemodynamic::Meds
#> # A tibble: 143 x 7
#>    Case_Date           Seq_nr New50_Over4Hr Generic_Drug_Na…  Dose Unit 
#>    <dttm>               <dbl>         <dbl> <chr>            <dbl> <chr>
#>  1 2011-01-19 00:00:00      1             0 EPHEDRINE SULFA…    10 mg   
#>  2 2011-01-19 00:00:00      1             0 EPHEDRINE SULFA…    10 mg   
#>  3 2010-01-06 00:00:00      3             0 EPHEDRINE SULFA…    10 mg   
#>  4 2010-02-12 00:00:00      6             0 EPHEDRINE SULFA…    10 mg   
#>  5 2010-04-06 00:00:00      8             1 EPHEDRINE SULFA…    10 mg   
#>  6 2010-04-06 00:00:00      8             1 EPHEDRINE SULFA…    20 mg   
#>  7 2011-01-31 00:00:00     11             0 EPHEDRINE SULFA…    10 mg   
#>  8 2011-01-31 00:00:00     11             0 EPHEDRINE SULFA…    10 mg   
#>  9 2011-01-31 00:00:00     11             0 ESMOLOL HCL         10 mg   
#> 10 2011-01-31 00:00:00     11             0 EPHEDRINE SULFA…     5 mg   
#> # ... with 133 more rows, and 1 more variable: Dose_St_Dttime <dttm>

hemodynamic::Labs
#> # A tibble: 1,845 x 6
#>    Case_Date           Seq_Nr New50_Over4Hr Timestamp           Lab_Test
#>    <dttm>               <dbl>         <dbl> <dttm>              <chr>   
#>  1 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |A…
#>  2 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |B…
#>  3 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |B…
#>  4 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |B…
#>  5 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |C…
#>  6 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |E…
#>  7 2010-01-21 00:00:00      2             0 2010-02-03 09:38:00 Labs |P…
#>  8 2010-01-21 00:00:00      2             0 2010-02-03 09:39:00 Blood G…
#>  9 2010-01-21 00:00:00      2             0 2010-02-03 09:39:00 Blood G…
#> 10 2010-01-21 00:00:00      2             0 2010-02-03 09:39:00 Blood G…
#> # ... with 1,835 more rows, and 1 more variable: Value <chr>

hemodynamic::IO
#> # A tibble: 4,562 x 5
#>    Case_Date           Seq_nr New50_Over4Hr Date_Obs            Value     
#>    <dttm>               <dbl>         <dbl> <dttm>              <chr>     
#>  1 2011-01-19 00:00:00      1             0 2011-01-19 09:49:20 0.9 NaCl …
#>  2 2011-01-19 00:00:00      1             0 2011-01-19 10:03:43 Midazolam…
#>  3 2011-01-19 00:00:00      1             0 2011-01-19 10:10:04 Fentanyl …
#>  4 2011-01-19 00:00:00      1             0 2011-01-19 10:10:10 Lidocaine…
#>  5 2011-01-19 00:00:00      1             0 2011-01-19 10:10:15 Propofol …
#>  6 2011-01-19 00:00:00      1             0 2011-01-19 10:18:50 Ephedrine…
#>  7 2011-01-19 00:00:00      1             0 2011-01-19 10:26:17 Propofol …
#>  8 2011-01-19 00:00:00      1             0 2011-01-19 10:26:38 Preoperat…
#>  9 2011-01-19 00:00:00      1             0 2011-01-19 10:31:44 Fentanyl …
#> 10 2011-01-19 00:00:00      1             0 2011-01-19 10:44:44 Ephedrine…
#> # ... with 4,552 more rows

hemodynamic::CASES
#> # A tibble: 12,090 x 7
#>      case date    time   m     m_value m_label    cases 
#>     <int> <chr>   <time> <fct>   <int> <chr>      <chr> 
#>  1 411470 6/29/11 16:26  HR        106 Heart rate 411470
#>  2 411470 6/29/11 16:26  HR        106 Heart rate 411470
#>  3 411470 6/29/11 16:27  HR        105 Heart rate 411470
#>  4 411470 6/29/11 16:27  HR        106 Heart rate 411470
#>  5 411470 6/29/11 16:28  HR        106 Heart rate 411470
#>  6 411470 6/29/11 16:28  HR        104 Heart rate 411470
#>  7 411470 6/29/11 16:29  HR        100 Heart rate 411470
#>  8 411470 6/29/11 16:29  HR        104 Heart rate 411470
#>  9 411470 6/29/11 16:30  HR        100 Heart rate 411470
#> 10 411470 6/29/11 16:30  HR         96 Heart rate 411470
#> # ... with 12,080 more rows

hemodynamic::PHYSICIANS
#> # A tibble: 40 x 4
#>    name     whole_score dominating_factors data            
#>    <chr>          <dbl> <list>             <list>          
#>  1 caruso             4 <chr [1]>          <tibble [3 × 6]>
#>  2 caruso             5 <chr [1]>          <tibble [3 × 6]>
#>  3 caruso             3 <chr [3]>          <tibble [3 × 6]>
#>  4 caruso             3 <chr [1]>          <tibble [3 × 6]>
#>  5 caruso             5 <chr [1]>          <tibble [3 × 6]>
#>  6 colbrown           5 <chr [1]>          <tibble [3 × 6]>
#>  7 colbrown           4 <chr [2]>          <tibble [3 × 6]>
#>  8 colbrown           6 <chr [3]>          <tibble [3 × 6]>
#>  9 colbrown           3 <chr [2]>          <tibble [3 × 6]>
#> 10 colbrown           6 <chr [2]>          <tibble [3 × 6]>
#> # ... with 30 more rows

hemodynamic::PHYSICIANS%>%dplyr::select(-dominating_factors)%>%tidyr::unnest(data)
#> # A tibble: 120 x 8
#>    name   whole_score block sub_block DomFactor End      Score Start   
#>    <chr>        <dbl> <int>     <int> <chr>     <chr>    <int> <chr>   
#>  1 caruso           4     1         1 % Change  18:25:00     5 16:25:00
#>  2 caruso           4     2         2 % Change  20:25:00     5 18:25:00
#>  3 caruso           4     3         1 % Change  21:05:00     5 20:25:00
#>  4 caruso           5     1         1 % Change  12:54:00     3 10:51:00
#>  5 caruso           5     2         2 % Change  15:08:00     3 12:54:00
#>  6 caruso           5     3         1 % Change  17:18:00     3 15:08:00
#>  7 caruso           3     1         1 % Change  08:27:00     5 08:02:00
#>  8 caruso           3     2         2 % Change  12:03:00     3 10:03:00
#>  9 caruso           3     3         1 % Change  12:13:00     3 12:03:00
#> 10 caruso           3     1         1 % Change  16:16:00     3 14:15:00
#> # ... with 110 more rows
```

Directory containing [raw data](data-raw):

### ORData\_151.xlsx

  - sheet: PtInfo
  - rows: 151
  - columns: 8
      - Case\_Date (c(“POSIXct”, “POSIXt”))
      - Seq\_nr (numeric)
      - New50\_Over4Hr (numeric)
      - Age (numeric)
      - ASA (character)
      - Weight (numeric)
      - Sched\_Proc\_Desc (character)
      - Preop\_Dx (character)
  - sheet: Meds
  - rows: 143
  - columns: 7
      - Case\_Date (c(“POSIXct”, “POSIXt”))
      - Seq\_nr (numeric)
      - New50\_Over4Hr (numeric)
      - Generic\_Drug\_Name (character)
      - Dose (numeric)
      - Unit (character)
      - Dose\_St\_Dttime (c(“POSIXct”, “POSIXt”))
  - sheet: Labs
  - rows: 1845
  - columns: 6
      - Case\_Date (c(“POSIXct”, “POSIXt”))
      - Seq\_Nr (numeric)
      - New50\_Over4Hr (numeric)
      - Timestamp (c(“POSIXct”, “POSIXt”))
      - Lab\_Test (character)
      - Value (character)
  - sheet: IO
  - rows: 4562
  - columns: 5
      - Case\_Date (c(“POSIXct”, “POSIXt”))
      - Seq\_nr (numeric)
      - New50\_Over4Hr (numeric)
      - Date\_Obs (c(“POSIXct”, “POSIXt”))
      - Value (character)

### ORData\_19.xlsx

  - sheet: CASE LIST
  - rows: 19
  - columns: 14
      - Case Date (c(“POSIXct”, “POSIXt”))
      - Obfuscated Case Number (numeric)
      - Obfuscated MRN (numeric)
      - Patient Name(Cases) (character)
      - Age (numeric)
      - Admit Weight (kg) (character)
      - ASA (character)
      - Site Code (character)
      - Room Name (character)
      - Preop Dx (character)
      - CPT 1 (character)
      - CPT 1 Description (character)
      - Postop Dx (logical)
      - Room Duration (numeric)
  - sheet: LABS
  - rows: 144
  - columns: 4
      - Obfuscated Case Number (numeric)
      - Lab Dt-Time (c(“POSIXct”, “POSIXt”))
      - Lab Name (character)
      - Lab Value (character)
  - sheet: MEDS
  - rows: 205
  - columns: 5
      - Obfuscated Case Number (numeric)
      - Med Description (character)
      - Med Start (c(“POSIXct”, “POSIXt”))
      - Total Amount (numeric)
      - Unit of Measure (character)
  - sheet: I\_O
  - rows: 68
  - columns: 5
      - Obfuscated Case Number (numeric)
      - I/O Description (character)
      - Start Datetime (c(“POSIXct”, “POSIXt”))
      - Total Amount (numeric)
      - Unit of Measure (character)
  - sheet: MONITOR
  - rows: 13000
  - columns: 4
      - Obfuscated Case Number (numeric)
      - Prompt (character)
      - Observation Datetime (c(“POSIXct”, “POSIXt”))
      - Value (character)
  - sheet: MONITOR 2
  - rows: 10954
  - columns: 4
      - Obfuscated Case Number (numeric)
      - Prompt (character)
      - Observation Datetime (c(“POSIXct”, “POSIXt”))
      - Value (character)

### Case\_Outcome\_159.csv

  - rows: 160
  - columns: 15
      - ACCT\_ID (integer)
      - CASE\_ID (character)
      - DIED (integer)
      - IP\_OP (character)
      - CASE\_VOL (integer)
      - LOS (integer)
      - ICU\_DAYS (integer)
      - ICD9\_ADMIT (character)
      - ICD9\_ADMIT\_LABEL (character)
      - ICD9\_CODE\_OUTCOME (character)
      - CARDIAC (integer)
      - BRAIN (integer)
      - KIDNEY (integer)
      - LIVER (integer)
      - PULMONARY (integer)

### Monitor\_151.csv

  - rows: 96488
  - columns: 5
      - case (integer)
      - obs\_index (integer)
      - time (c(“hms”, “difftime”))
      - variable (character)
      - value (integer)

### Monitor\_19.csv

  - rows: 16116
  - columns: 6
      - case (integer)
      - date (character)
      - time (c(“hms”, “difftime”))
      - m (integer)
      - m\_value (integer)
      - m\_label (character)

### survey\_curated\_data.csv

  - rows: 23954
  - columns: 4
      - Case (integer)
      - Variable (character)
      - Date (character)
      - Value (integer)

### survey\_raw\_data.csv

  - rows: 16116
  - columns: 7
      - cases (integer)
      - date (character)
      - times (c(“hms”, “difftime”))
      - m (integer)
      - mv (integer)
      - iter (integer)
      - m.label (character)

### Physicians/\*.xml

  - survey results
